# Codes R pour le calcul de la vibrisses avec des données < LQ


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#Introduction)
- [Objectif](#Objectif)
- [Vibrisse de Tukey et limites de quantification](#Vibrisse-de-Tukey-et-limites-de-quantification)
- [description des scripts](#description-des-scripts)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

Ce projet github rassemble les scripts R produits dans le cadre du projet FGU III qui est piloté par le BRGM pour la production d'information sur le fond pédogéochimique urbain.

## Objectif

Un travail de réflexion sur la détermination des valeurs de fond et la procédure générale de leur restitution via l’outil BRGM ORCIA a été entrepris entre les membres du consortium statisticiens et géostatisticiens (S. Belbeze, C. De Fouquet, H. Demougeot-Renard, N. Saby, B. Sauvaget). 

## Vibrisse de Tukey et limites de quantification

Chantal de Fouquet
Mines Paris-Tech
8 septembre 2021


Une solution pragmatique, voire simpliste, est proposée pour le calcul de la vibrisse de Tukey,lorsque le taux de mesures non quantifiées dépasse 25%. A condition d'introduire un vecteur de
poids, remplacer chaque donnée non quantifiée par k valeurs discrétisées (et non pas simulées)
entre 0 et LQ, chacune recevant (pour cette donnée) un poids 1/k. Chaque donnée quantifiée reçoit
un poids égal à 1. Ensuite, la vibrisse de Tukey est calculée avec les données pondérées (initiales et
reconstituées).
Une nuance est introduite, car il a été demandé d'envisager deux cas (LQ ou 1.5LQ) lorsque 100%
des données sont non quantifiées. Dans ce cas limite, pour retrouver LQ par la vibrisse de Tukey,
l’intervalle (0, LQ) est divisé en trois, en se ramenant à la loi uniforme dans chacun de ces segments.

## description des scripts

l'implémentation de la méthode est disponible dans le script `Test_LQ_script.R`.

Les autres scripts disponible dans le répertoire `test` utilisent la méthode implémentée par Baptiste Sauvaget pour des simulations afin de tester l'effet des différentes tailles d'échantillon et de
proportions de valeurs inférieurs à la LQ.



## Exemple de sortie
Voici la sortie du script `Test_LQ_script.R`

![](img/test.png)



