
# Objectif : Tester les proposition de Chantal pour le calcul de valeurs de fond dans BDSolU
# en tenant compte des LQ

# Cas d'etude : Toulouse

# Baptiste Sauvaget / Nicolas Saby : 09/2021


# Partie 1: Chargement des donnees et des packages-----


library(fitdistrplus)
library(dplyr)
library(foreach)
library(tidyr)
# On definit le dossier de travail et les packages ? charger

#Adresse du dossier de travail

# On importe les donnees d'analyses
# save(res1,file="PourTest.RData")
load("data/PourTest.RData")
Analyses <- res1[[1]] %>%
  filter(occ == "TtesOccupations")

DL <- res1[[2]] %>%
  filter(occ == "TtesOccupations")

# Fonction de statistiques ?l?mentaires
source('src/Statelem.R')
library(ggplot2)
library(reldist)

# Fonction de calcul des vibrisses
vib <- function(elt,coef) {
  
  if(is.numeric(coef)) {
    
    r <- quantile(elt,probs=0.75)+
      coef*(quantile(elt,probs=0.75)-
              quantile(elt,probs=0.25))
    
  } else {
    r <- quantile(elt,probs=0.75)+
      3*(quantile(elt,probs=0.75)-
           quantile(elt,probs=0.5))
    
  }
  
  return(r)
} 


# Partie 2: Estimation de la distribution et simulation  -----


elt.name = "fluoranthene"
# Remplissage de la matrice de stats ?l?mentaires
Statistiques=Statelem(Analyses[,elt.name])
Statistiques

DLelt <- max(Analyses[,elt.name][ DL[,elt.name]=='<'],na.rm=T )
elt <- Analyses[!is.na(Analyses[,elt.name]),elt.name]

plotdist(elt[elt>DLelt], histo = TRUE, demp = TRUE)
descdist(elt[elt>DLelt], boot = 1000)


fg <- fitdist(elt[elt>DLelt], "gamma")
fln <- fitdist(elt[elt>DLelt], "lnorm")
fw <- fitdist(elt[elt>DLelt], "weibull")

 # par(mfrow = c(2, 2))
  plot.legend <- c("Weibull", "lognormal", "gamma")
 # 
 # denscomp(list(fw, fln,fg), legendtext = plot.legend)
 # qqcomp(list(fw, fln,fg), legendtext = plot.legend)
 # cdfcomp(list(fw, fln,fg), legendtext = plot.legend)
 # ppcomp(list(fw, fln,fg), legendtext = plot.legend)
 # par(mfrow = c(1, 1))
 
 # simulations
 nbsimu = 100000
 
 vecTot = rlnorm(n=nbsimu,meanlog = fln$estimate[1],sdlog = fln$estimate[2])
 # vec = rgamma(n=nbsimu,shape = fg$estimate[1],rate =  fg$estimate[2])
 
 
 # # Avec les données Toulouse
 # Analyses <- read.table("données_test_LQ .txt",header=TRUE,sep=";") 
 # elt.name = 'Pb'
 #  vec <- Analyses[,elt.name]
 # DLelt <- max(Analyses[,elt.name][ Analyses[,'LQ_Pb']==0],na.rm=T )
 # nbsimu = length(Analyses[,elt.name])
 # 
 Analyses <- read.table("../Tests_LQ_export/Tests_LQ_export/données_test_LQ .txt",header=TRUE,sep=";") 
 vecTot <- Analyses$Cu
 # Boucle par n-----------
 listnbobs = c(30,50,75,100,150,200,500,1000,2000,5000)
 listnbobs = c(30,50,75,100,150,200,500,1000,1250,1600)
 
 resG <- foreach(krep = 1:30,
                 .combine = rbind.data.frame
 ) %do%{
   print(krep)
   
   restmp <- foreach(nbobs = listnbobs,
                 .combine = rbind.data.frame
                 ) %do%{
                   
                   # création du vecteur de données
                   vec = vecTot[1:nbobs]
                   
                   # Partie 3: Calculs des vibrisses sans données censurees ----
                   #(Sans ajouter des donn?es <LQ)
                   
                   Statistiques=Statelem(vec)
                   names(Statistiques)=c('Nb','Min','Max','Mean','Std','CV','Q5','Q10','Q25','Median','Q75','Q90','Q95')
                   
                   
                   # Calculs des vibrisses brutes (classique et m?diane)
                   Vib_calc  <- c(
                     vib(vec,coef = 1.5),
                     vib(vec,"med")
                   )
                   
                   names(Vib_calc)=c("Vib_classique","Vib_median")
                   
                   
                   
                   # on cr?e un vecteur quantile (tout les 5) et le champs nom associ? pour les vibrisses
                   quant=seq(0,100,5)
                   for (k in 1:(21)){
                     quant[k]= paste("Vib_cens_",quant[k],sep="")
                   }
                   
                   
                   
                   
                   # Partie 4 : Censure des donn?es  ----
                   # ? diff?rents quantiles 
                   
                   
                   #Remplissage de la matrice des quantiles
                   Quantiles=quantile(vec,probs=seq(0,1,0.05))
                   names(Quantiles)=paste0('q',seq(0,100,5))
                   
                   
                   # cr?ation  des matrices censur?es par param?tre (
                   
                   
                   matrice_censure <- foreach(j = 1:21,
                                              .combine= cbind.data.frame)%do%{
                                                MesDL = sample(c(Quantiles[j],Quantiles[j]/2),
                                                               size= length(vec),
                                                               replace=T
                                                               )
                                                
                                                # MesDL = Quantiles[j]
                                                # vec*(vec>MesDL)
                                                MesDL
                                              }
                   
                   colnames(matrice_censure)=paste0('q',seq(0,100,5))
                   
                   
                   # Remplacer la dl par 0-----------
                   matrice_censure_0 <- vec * (vec > matrice_censure)
                   
                   #Calcule des vibrisses (les 2 m?thodes) sur les donn?es censur?es
                   
                   Vib_calc_censure_0 <- foreach(i = 1:21,
                                                 .combine= cbind.data.frame)%do%{
                                                   rbind(vib(matrice_censure_0[,i],1.5),
                                                         vib(matrice_censure_0[,i],"med"))
                                                   
                                                 }
                   
                   
                   
                   colnames(Vib_calc_censure_0)=quant
                   
                   
                   # Partie 5 : substitution par la LQ ----
                   
                   #Cr?ation et remplissage des matrice de subsitution des <LQ par la LQ (?volutive)

                   matrice_censure_substi <- apply(matrice_censure,
                                                   2,
                                                   function(i){
                                                     ifelse(vec>i,vec,i)
                                                     }
                                                   )
                   

                   
                   
                   
                   #Calcule des vibrisses (les 2 m?thodes) sur les donn?es censur?es et substitu?es par LQ
                   
                   Vib_calc_censure_LQ <- foreach(i = 1:21,
                                                  .combine= cbind.data.frame)%do%{
                                                    
                                                    rbind(vib(matrice_censure_substi[,i],1.5),
                                                          vib(matrice_censure_substi[,i],"med")
                                                    )
                                                    
                                                    
                                                    
                                                  }
                   # Partie 6 : Calcule sur donn?es log-translat?es ----
                   
                   vec_log=log(vec+1)
                   
                   # Calculs des vibrisses sur les log (classique et m?diane)
                   Vib_calc_log = c(
                     vib(vec_log,1.5),
                     vib(vec_log,"med")
                   )
                   
                   
                   Vib_calc_log_back=(exp(Vib_calc_log))-1
                   
                   
                   # Partie 7 : tirage dans une loi uniforme ----
                   
                   
                   # Remplissage de la matrice avec des tirage al?atoire d'une loi uniforme entre 0 et LQ pour le cuivre
                   
                   Tirage <- foreach(i = 1:21,
                                     .combine= cbind.data.frame)%do%{
                                       v1 = runif(nbobs, min=0, max = Quantiles[i])
                                       v2 = runif(nbobs, min=0, max = Quantiles[i]/2)
                                       
                                       ifelse(vec > matrice_censure[,i],
                                              vec,
                                              ifelse(matrice_censure[,i] > Quantiles[i]/2,
                                                     v1,
                                                     v2)
                                              )
                                     }
                   colnames(Tirage)=paste0('q',seq(0,100,5))
                   
                   
                   
                   
                   #Cr?ation et remplissage des matrice de substitution des <LQ par le tirage al?atoire (?volutif)
                   matrice_censure_substi_runif <- Tirage

                   
                   
                   #Calcule des vibrisses (les 2 m?thodes) sur les donn?es censur?es et substitu?es par le tirage al?atoire
                   
                   Vib_calc_censure_runif <- foreach(i = 1:21,
                                                     .combine= cbind.data.frame)%do%{
                                                       
                                                       rbind(vib(matrice_censure_substi_runif[,i],1.5),
                                                             vib(matrice_censure_substi_runif[,i],"med")
                                                       )
                                                     }
                   
                   
                   colnames(Vib_calc_censure_runif)=paste0('q',seq(0,100,5))
                   
                   
                   
                   
                   # Partie 8 : Discrétisation -----
                   
                   Discretisation_10 <- foreach(i = 1:21,
                                                .combine= cbind.data.frame)%do%{
                                                  seq(from=0,to=Quantiles[i],by=(Quantiles[i]/10))
                                                }
                   
                   colnames(Discretisation_10)=paste0('q',seq(0,100,5))
                   
                   
                   # Élimination des valeurs inférieures ? la LQ
                   Matrice_censure_NA=matrice_censure_0
                   Matrice_censure_NA[Matrice_censure_NA == 0] = NA
                   colnames(Matrice_censure_NA)=paste0('q',seq(0,100,5))
                   
                   # Ajout des vecteurs de discr?tisation en fin de colonnes
                   Matrice_censure_NA=rbind(Matrice_censure_NA,Discretisation_10)
                   
                   
                   #calcul des ponderation par quantile (vecteur de 21, puis matrice de 11 par 21)
                   Poids_LQ=colSums(matrice_censure_0==0)
                   
                   Poids_LQ=Poids_LQ/11
                   
                   
                   Poids_LQ=matrix(Poids_LQ,nrow=11,ncol=21,byrow=T)
                   
                   # cr?ation de la matrice de pond?ration 
                   
                   matrice_poids=matrice_censure_0
                   matrice_poids[matrice_poids>0]=1
                   colnames(matrice_poids)=paste0('q',seq(0,100,5))
                   colnames(Poids_LQ)=paste0('q',seq(0,100,5))
                   
                   matrice_poids=rbind.data.frame(matrice_poids,Poids_LQ)
                   
                   
                   # Calcul des vibrisses pond?r?es
                   
                   Vib_calc_censure_disc1.5 <- foreach(i = 1:21,
                                                       .combine= cbind.data.frame)%do%{
                                                         wtd.quantile(
                                                           Matrice_censure_NA[,i],
                                                           weight=matrice_poids[,i],q=0.75,na.rm=T)+
                                                           1.5*(
                                                             wtd.quantile(Matrice_censure_NA[,i],
                                                                          weight=matrice_poids[,i],q=0.75,na.rm=T)-
                                                               wtd.quantile(Matrice_censure_NA[,i],
                                                                            weight=matrice_poids[,i],q=0.25,na.rm=T)
                                                           )
                                                       }
                   
                   Vib_calc_censure_disc3 <- foreach(i = 1:21,
                                                     .combine= cbind.data.frame)%do%{
                                                       wtd.quantile(
                                                         Matrice_censure_NA[,i],
                                                         weight=matrice_poids[,i],q=0.75,na.rm=T)+
                                                         3*(
                                                           wtd.quantile(Matrice_censure_NA[,i],
                                                                        weight=matrice_poids[,i],
                                                                        q=0.75,
                                                                        na.rm=T)-
                                                             wtd.quantile(Matrice_censure_NA[,i],
                                                                          weight=matrice_poids[,i],
                                                                          q=0.5,
                                                                          na.rm=T)
                                                         )
                                                     }
                   
                   
                   Vib_calc_censure_disc <- rbind(Vib_calc_censure_disc1.5,Vib_calc_censure_disc3)
                   colnames(Vib_calc_censure_disc)=paste0('q',seq(0,100,5))
                   
                   
                   
                   

                   
                   
                   
                   ## Synthèse des résultats ----
                   df_Vib_calc_censure_0=as.data.frame(t(Vib_calc_censure_0[,c(1:20)]))
                   df_Vib_calc_censure_LQ=as.data.frame(t(Vib_calc_censure_LQ[,c(1:20)]))
                   df_Vib_calc_censure_runif=as.data.frame(t(Vib_calc_censure_runif[,c(1:20)]))
                   df_Vib_calc_censure_disc=as.data.frame(t(Vib_calc_censure_disc[,c(1:20)]))
                   colnames(df_Vib_calc_censure_0)<- c('vibClas','VibMed')
                   colnames(df_Vib_calc_censure_LQ)<- c('vibClas','VibMed')
                   colnames(df_Vib_calc_censure_runif)<- c('vibClas','VibMed')
                   colnames(df_Vib_calc_censure_disc)<- c('vibClas','VibMed')
                   
                   
                   
                   res <- rbind.data.frame(
                     cbind.data.frame(alg='1.Par0', Prop_LQ=seq(0,95,5) ,df_Vib_calc_censure_0  ),
                     cbind.data.frame(alg='2.LQ', Prop_LQ=seq(0,95,5) ,df_Vib_calc_censure_LQ),
                     cbind.data.frame(alg='3.tirages', Prop_LQ=seq(0,95,5) ,df_Vib_calc_censure_runif),
                     cbind.data.frame(alg='4.Discrét.', Prop_LQ=seq(0,95,5) ,df_Vib_calc_censure_disc)
                   ) %>%
                     gather(key,value,-alg,-Prop_LQ) %>%
                     mutate(diff = ifelse(key == "vibClas" , 
                                          abs(value - Vib_calc[1]) /  Vib_calc[1] , 
                                          abs(value - Vib_calc[2]) /  Vib_calc[2]
                                          )
                     )
                   
                   cbind.data.frame( nbobs , res )
   
                 }
   # replicate ------
   cbind.data.frame( krep , restmp )
   
 }


 ggplot(resG, 
        aes(x = factor(Prop_LQ),
            100 * diff,
            col=factor(key) ))+ 
   geom_boxplot(outlier.size = 1)+
   geom_abline(intercept = 50,slope = 0)+
   # geom_point(size=2)+ 
   # scale_color_manual(values=c('red','black'))+
   facet_grid(nbobs~alg)+ylim(0,150)+
   ylab(label = "Erreur relative en  %") + 
   xlab("Proportion de val < LQ %")+
   theme_bw() + 
   theme(axis.text.x = element_text(size = 6))

 
 
 ggplot(resG %>% filter(krep==2), 
        aes(x = Prop_LQ,
            100 * diff,
            col=factor(key) ))+ 
   geom_line()+
   geom_abline(intercept = 50,slope = 0)+
   # geom_point(size=2)+ 
   # scale_color_manual(values=c('red','black'))+
   facet_grid(nbobs~alg)+ylim(0,150)+
   ylab(label = "Erreur relative en  %") + xlab("Proportion de val < LQ %")
   theme_bw() + theme(axis.text.x = element_text(size = 6))
 
 
